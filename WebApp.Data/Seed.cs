﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApp.Data.Entities;

namespace WebApp.Data
{
    public class Seed
    {
        public static async Task SeedUser(DataContext context)
        {
            if (await context.Users.AnyAsync()) return;

            var dataUser = await File.ReadAllTextAsync("./userSeed.json");
            var user = JsonSerializer.Deserialize<List<User>>(dataUser);

            foreach (var item in user)
            {
                using var hmac = new HMACSHA512();

                item.UserName = item.UserName.ToLower();
                item.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes("Password@123"));
                item.PasswordSalt = hmac.Key;

                context.Users.Add(item);
            }
            await context.SaveChangesAsync();
        }
    }
}
