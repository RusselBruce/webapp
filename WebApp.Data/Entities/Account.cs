﻿using System.Collections.Generic;
using WebApp.Data.Entities;

namespace WebApp.Data
{
    public class Account
    {
        public int AccountId { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public List<Payment> Payments { get; set; }
        public int UserId { get; set; }

    }
}
