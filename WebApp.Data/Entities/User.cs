﻿using System.Collections.Generic;

namespace WebApp.Data.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
