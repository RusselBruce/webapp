﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApp.DTOs;
using WebApp.Extensions;
using WebApp.Repository.IRepository;

namespace WebApp.Controllers
{
    [Authorize]
    public class AccountController : BaseApiController
    {
        private readonly IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }
        [HttpGet]
        public async Task<ActionResult<AccountDto>> GetAccount()
        {
            return await _accountRepository.GetAccountAsync(User.GetUserName());
        }
    }
}
