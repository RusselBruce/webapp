﻿using WebApp.Data.Entities;

namespace WebApp.Repository.IRepository
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}
