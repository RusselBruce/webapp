﻿using System.Threading.Tasks;
using WebApp.Data;
using WebApp.Data.Entities;
using WebApp.DTOs;

namespace WebApp.Repository.IRepository
{
    public interface IAccountRepository
    {
        Task<AccountDto> GetAccountAsync(string username);
    }
}
