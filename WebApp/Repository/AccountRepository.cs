﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Data;
using WebApp.DTOs;
using WebApp.Repository.IRepository;

namespace WebApp.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly DataContext _context;
        public AccountRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<AccountDto> GetAccountAsync(string username)
        {
            var user = await _context.Users.Where(x => x.UserName == username).SingleOrDefaultAsync();

            return await _context.Accounts
            .Include(pay => pay.Payments.OrderByDescending(pay => pay.Date))
            .Where(a => a.UserId == user.Id)
            .Select( p => new AccountDto
            {
                AccountId = p.AccountId,
                Status = p.Status,
                Balance = p.Balance,
                Reason = p.Reason,
                Payments = p.Payments.Select(rn => new PaymentDto { Amount = rn.Amount, Date = rn.Date }).ToList()
            }).SingleOrDefaultAsync();
        }
    }
}
