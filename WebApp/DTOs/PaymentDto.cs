﻿using System;

namespace WebApp.DTOs
{
    public class PaymentDto
    {
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }
}
