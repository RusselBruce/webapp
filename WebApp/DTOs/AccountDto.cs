﻿using System.Collections.Generic;

namespace WebApp.DTOs
{
    public class AccountDto
    {
        public int AccountId { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public List<PaymentDto> Payments { get; set; }
    }
}
