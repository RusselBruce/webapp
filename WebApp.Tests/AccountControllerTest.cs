﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApp.Controllers;
using WebApp.DTOs;
using Xunit;


namespace WebApp.Tests
{
    public class AccountControllerTest
    {
        const string baseUrl = "http://localhost:5000/";
        [Fact]
        public async Task GetAccountSuccess()
        {
            using (var client = new HttpClient())
            {
                UserDto userDto = await UserLogin();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userDto.Token);
                HttpResponseMessage response = await client.GetAsync(baseUrl + "api/account"); //call the api from controller
                var json = await response.Content.ReadAsStringAsync();
                AccountDto accountDto = JsonConvert.DeserializeObject<AccountDto>(json);

                response.EnsureSuccessStatusCode();
                response.StatusCode.Should().Be(HttpStatusCode.OK);

                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }
        [Fact]
        public async Task GetAccountFailed()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", string.Empty);
                HttpResponseMessage response = await client.GetAsync(baseUrl + "api/account");  //call the api from controller

                response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }
        public async Task<UserDto> UserLogin()
        {
            //user need to login first, so he can view his account.
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.PostAsync(baseUrl + "api/user/login", JsonContent.Create(new LoginDto() { UserName = "user", Password = "Password@123" }));
                var json = await response.Content.ReadAsStringAsync();
                UserDto userDto = JsonConvert.DeserializeObject<UserDto>(json);
                return userDto;
            }
        }
    }
}
