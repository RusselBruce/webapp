﻿using FluentAssertions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApp.DTOs;
using Xunit;

namespace WebApp.Tests
{
    public class UserControllerTest
    {
        const string baseUrl = "http://localhost:5000/";
        [Fact]
        public async Task LoginAsyncSuccess()
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.PostAsync(baseUrl + "api/user/login", JsonContent.Create(new LoginDto() { UserName = "user", Password = "Password@123" }));
                response.EnsureSuccessStatusCode();
                response.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
        [Fact]
        public async Task LoginAsyncFailed()
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.PostAsync(baseUrl + "api/user/login", JsonContent.Create(new LoginDto() { UserName = "usera", Password = "Password@123" }));
                response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }
    }
}
