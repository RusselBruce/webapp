# README #

### CODE TEST - BE ###

### Tech ###
- Visual studio
- SQLite (install SQLite/SQL Server Compact Toolbox extension)
- Security/Auth(JWT)
- XUnit (Testing)
- Postman

### List of assumptions: ###
1. There's 2 users (1  "Open" status and 1 "Closed" status).
2. The status field is in the account level
3. The user will only view his/her payments and account balance, in this case there's no operation of adding or subtracting from balance based on payments.
4. The default status is "Open", if balance is 0 the account status is "Closed".
5. Reason field will only have value when status is "Closed".
6. There's an existing data for the user by seeding the database.
7. The user need to login, so he/she may able to view the account

### Files included ###
1. YAML file for CI config
2. Postman_collection for API Testing (user login, view account)

### CI Configuration ###
1. Create project in azure devops repository.
2. Generate pipeline, then choose Bitbucket Cloud as source.
3. Select this repository as and master as branch.
4. Choose the ASP.NET Core as template.
5. The unit test project is using a database connection, that's why I removed the Test from agent job.
5. Click the save and queue > save and run the pipeline.
 
### Azure Pipeline Tag ###
[![Build status](https://dev.azure.com/rbpcipriano/WebAppBackEnd/_apis/build/status/WebAppBackendTest)](https://dev.azure.com/rbpcipriano/WebAppBackEnd/_build/latest?definitionId=6)